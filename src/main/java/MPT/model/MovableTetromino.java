package MPT.model;

import MPT.tetrominos.Tetromino;

public class MovableTetromino {
    private Tetromino piece;
    private String name;
    private Integer[][] shape;
    private int state;
    private Position position;
    private boolean i_gimmik;

    public MovableTetromino(Tetromino piece, Position position) {
        this.piece = piece;
        this.position = new Position(position.getX(), position.getY());
        name = this.piece.getClass().getName();
        shape = piece.turn(-1);
        i_gimmik = false;
        state = 0;
    }

    public Tetromino getPiece() {
        return piece;
    }

    public String getName() {
        return name;
    }

    public Integer[][] getShape() {
        return shape;
    }

    public void setShape(Integer[][] shape) {
        this.shape = shape;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Position getPosition() {
        return position;
    }

    public boolean getI_gimmik () {
        return i_gimmik;
    }

    public void setI_gimmik(boolean i_gimmik) {
        this.i_gimmik = i_gimmik;
    }
}
