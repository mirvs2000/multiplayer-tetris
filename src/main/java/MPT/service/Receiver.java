package MPT.service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Receiver extends Thread{
    private final ConcurrentLinkedQueue<Message> toModel;
    private final DatagramSocket socket;
    byte[] buf;
    private final Object connectionLock;
    private final long connectionTimeout = 3000;
    private boolean isConnected = false;

    public Receiver(DatagramSocket socket) throws SocketException {
        super();
        toModel = new ConcurrentLinkedQueue<>();
        this.socket = socket;
        buf = new byte[1 << 16];
        connectionLock = new Object();
    }

    public boolean waitForConnection(){
        boolean connected = true;
        long time = System.currentTimeMillis();
        synchronized (connectionLock){
            try {
                while (System.currentTimeMillis() - time <= connectionTimeout &&
                        !isConnected)
                    connectionLock.wait(connectionTimeout);
                if (!isConnected)
                    connected = false;
            }
            catch (InterruptedException e){
                if (!isConnected)
                    connected = false;
            }
        }

        return connected;
    }

    public void kill(){
        socket.close();
    }

    @Override
    public void run() {
        while (!isInterrupted()){
            try {
                // 1. Reading UDP packet
                DatagramPacket packet = new DatagramPacket(buf, 0, buf.length);
                socket.receive(packet);
                //System.out.println("Got packet from " + packet.getPort());

                // 2. Form message representation (???)
                Message msg = Protocol.extractMessage(buf);

                // 2.1. Check if connection
                if (msg != null && msg.getMsgType() == Protocol.JOIN){
                    synchronized (connectionLock){
                        toModel.add(msg);
                        // to not to add msg twice to queue
                        msg = null;
                        isConnected = true;
                        connectionLock.notify();
                    }
                }

                // 3. Place message representation at `toModel`
                if (msg != null)
                    toModel.add(msg);
            }
            catch (IOException e){
                break;
            }
        }
    }

    public Message poll(){
        return toModel.poll();
    }
}
