package test.java;

import MPT.model.Model;
import MPT.service.Message;
import MPT.service.Protocol;
import MPT.service.Sender;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import javafx.util.Pair;
import org.junit.jupiter.api.function.ThrowingSupplier;

import static org.junit.jupiter.api.Assertions.*;

public class MessagingTests {
    /*
    @Test
    @DisplayName("Sender test")
    public void SenderTest(){
        try {
            MPT.service.Sender sender = new Sender(InetAddress.getByName("localhost"), new DatagramSocket());
            sender.putMessage(null);
            sender.putMessage(new Message((byte) 0, null));
            sender.setGameId((byte) 0);
            sender.setGameId((byte) 1);
        }
        catch (UnknownHostException e){
            e.printStackTrace();
        }
        catch (SocketException e){
            e.printStackTrace();
        }
    }

     */

    @Test
    @DisplayName("Protocol test")
    public void ProtocolTest(){
        byte[] buf = new byte[2];
        buf[0] = Protocol.JOIN;
        buf[1] = 0x05;
        Message msg = Protocol.extractMessage(buf);
        assertEquals(msg.getMsgType(), Protocol.JOIN);
        assertEquals((byte)msg.getData(), 0x05);

        buf[0] = Protocol.COMBO_RECVD;
        msg = Protocol.extractMessage(buf);
        assertEquals(msg.getMsgType(), Protocol.COMBO_RECVD);

        buf = new byte[206];
        buf[0] = Protocol.PLAYERS_FIELD;
        buf[1] = 0x07;
        System.arraycopy(ByteBuffer.allocate(4).putInt(100).array(), 0, buf, 202, 4);
        for (int i = 2; i < 202; ++i)
            buf[i] = (byte) ((byte)i % 7);

        msg = Protocol.extractMessage(buf);
        Protocol.FieldTriplet triplet = (Protocol.FieldTriplet) msg.getData();
        byte id = triplet.getId();
        int score = triplet.getScore();
        ArrayList<Byte> field = triplet.getField();

        assertEquals(id, 0x07);
        assertEquals(score, 100);
        assertEquals(field.size(), 200);

        for (int i = 0; i < 200; ++i)
            assertEquals(buf[i + 2], (int)field.get(i));

        buf = new byte[9];
        buf[0] = Protocol.START_GAME;
        System.arraycopy(ByteBuffer.allocate(Long.BYTES).putLong(1l << 63).array(), 0, buf, 1, Long.BYTES);
        msg = Protocol.extractMessage(buf);
        assertEquals((long)msg.getData(), 1l << 63);
    }

    @Test
    @DisplayName("Sender test")
    public void senderTest(){

        try {
            assertDoesNotThrow(()->InetAddress.getByName("localhost"));
            assertDoesNotThrow((ThrowingSupplier<DatagramSocket>) DatagramSocket::new);
            Sender sender = new Sender(InetAddress.getByName("localhost"), new DatagramSocket());
            sender.start();

            byte[] buf = new byte[200];
            for (int i = 0 ; i < 200; ++i)
                buf[i] = (byte) (i & 1);
            Message msg = new Message(Protocol.PLAYERS_FIELD, buf);
            sender.putMessage(msg);

            buf = new byte[10];
            for (byte i = 0; i < 10; ++i)
                buf[i] = i;
            msg = new Message(Protocol.COMBO, new Pair<Integer, Integer>(10, 10));
            //msg = new Message(Protocol.COMBO, buf);
            sender.putMessage(msg);

            msg = new Message(Protocol.COMBO_RECVD, null);
            sender.putMessage(msg);

            msg = new Message(Protocol.READINESS, null);
            sender.putMessage(msg);

            msg = new Message(Protocol.START_GAME, null);
            sender.putMessage(msg);

            sender.putMessage(new Message(Protocol.KILL_SENDER, null));
            sender.join();

            //Thread.sleep(5000);
            //sender.kill();
        }
        catch (UnknownHostException | SocketException | InterruptedException e){
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("`handleMessage` test")
    public void handleMessageTest(){
        MPT.model.Model model = new Model();
        ArrayList<Byte> field = new ArrayList<>();
        for (int i = 0; i < 200; ++i)
            field.add((byte) 0x01);
        Protocol.FieldTriplet triplet = new Protocol.FieldTriplet(field, (byte)10, 10);
        Message msg = new Message(Protocol.PLAYERS_FIELD, triplet);
        model.handleMessage(msg);

        msg = new Message(Protocol.JOIN, (byte)1);
        model.handleMessage(msg);

        msg = new Message(Protocol.COMBO, 10);
        model.handleMessage(msg);

        msg = new Message(Protocol.COMBO_RECVD, 10);
        model.handleMessage(msg);

        msg = new Message(Protocol.START_GAME, 1l);
        model.handleMessage(msg);
    }
}
