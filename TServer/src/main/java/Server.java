import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class Server {

    private final int socketUDP = 7000;
    //private final int socketTCP = 6000;

    private byte[] receivingDataBufferUDP = new byte[1024];
    private byte[] sendingDataBufferUDP = new byte[1024];

    private byte[] receivingComboBufferUDP = new byte[1024];
    private byte[] sendingComboBufferUDP = new byte[1024];

    private DatagramSocket serverSocketComboUDP;
    private DatagramPacket inputPacketComboUDP;


    //private byte[] receivingDataBufferTCP = new byte[1024];
    //private byte[] sendingDataBufferTCP = new byte[1024];

    //ServerSocket serverSocketTCP;
    //Socket clientConnTCP;
    //DataOutputStream serverOutputTCP;
    //DataInputStream serverInputTCP;

    private DatagramSocket serverSocketUDP;
    private DatagramPacket inputPacketUDP;

    public Server() {
        try {
            serverSocketUDP = new DatagramSocket(socketUDP);
            //serverSocketTCP = new ServerSocket(socketTCP);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public Server(int socket) {
        try {
            serverSocketUDP = new DatagramSocket(socket);
            //serverSocketTCP = new ServerSocket(socketTCP);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public void joiningGameUDP(Player player, byte id){
        try {
            player.setId(id);
            for (int i = 0; i < 1023 ; i++){
                receivingDataBufferUDP[i] = receivingDataBufferUDP[i + 1];
            }
            player.setAddress(inputPacketUDP.getAddress());
            player.setPort(inputPacketUDP.getPort());
            sendingDataBufferUDP[0] = 0x00;
            sendingDataBufferUDP[1] = id;
            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBufferUDP, sendingDataBufferUDP.length,
                    player.getAddress(),player.getPort()
            );
            serverSocketUDP.send(outputPacket);

        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public boolean addressPortUDP(ArrayList<Player> players, byte id){
        boolean b = false;
        for (int i = 1; i < id ; i++) {
            if(players.get(i).getAddress() == inputPacketUDP.getAddress() && players.get(i).getPort() == inputPacketUDP.getPort()){
                b = true;
            }
        }
        return b;
    }

    public void receivingUTP(){
        try {
            inputPacketUDP = new DatagramPacket(receivingDataBufferUDP, receivingDataBufferUDP.length);
            serverSocketUDP.receive(inputPacketUDP);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public byte[] getReceivingDataBufferUDP(){
        return receivingDataBufferUDP;
    }

    public void playerField(Player player){
        try {
            sendingDataBufferUDP = receivingDataBufferUDP;
            sendingDataBufferUDP[0] = 0x01;
            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBufferUDP, sendingDataBufferUDP.length,
                    player.getAddress(),player.getPort()
            );
            serverSocketUDP.send(outputPacket);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public void start(Player player){
        try {
            byte[] random;
            random = longToBytes(System.currentTimeMillis());
            for (int i = 1; i <= 8 ; i++){
                sendingDataBufferUDP[i] = random[i-1];
            }
            sendingDataBufferUDP[0] = 0x05;
            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBufferUDP, sendingDataBufferUDP.length,
                    player.getAddress(),player.getPort()
            );
            serverSocketUDP.send(outputPacket);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }

    }

    public void combo(Player player, byte[] data){
        try {
            sendingDataBufferUDP[0] = 0x02;
            for (int i = 6; i < 10 ; i++){
                sendingDataBufferUDP[i - 5] = data[i];
            }
            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBufferUDP, sendingDataBufferUDP.length,
                    player.getAddress(),player.getPort()
            );
            serverSocketUDP.send(outputPacket);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public void comboReceived(Player player){
        try {
            sendingDataBufferUDP[0] = 0x03;
            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBufferUDP, sendingDataBufferUDP.length,
                    player.getAddress(),player.getPort()
            );
            serverSocketUDP.send(outputPacket);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }

    public byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    /*public void TCP(){
        try {
            clientConnTCP = serverSocketTCP.accept();
            serverOutputTCP = new DataOutputStream(clientConnTCP.getOutputStream());

        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }*/

    /*public void generatedSequence(Player player){
        try {
            sendingDataBufferUDP[0] = 0x04;
            LinkedList<Integer> sequence = rng.getSequence();
            for(int i = 1; i <= 7; i++){
                Integer a = sequence.get(i - 1);
                sendingDataBufferUDP[i] = a.byteValue();
            }
            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBufferUDP, sendingDataBufferUDP.length,
                    player.getAddress(),player.getPort()
            );
            serverSocketUDP.send(outputPacket);
            }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }*/

    /*public byte[] getReceivingComboBufferUDP(){
        return receivingComboBufferUDP;
    }*/

        /*public void receivingComboUTP(){
        try {
            inputPacketComboUDP = new DatagramPacket(receivingComboBufferUDP, receivingComboBufferUDP.length);
            serverSocketUDP.receive(inputPacketComboUDP);
        }
        catch(IOException e) {
            System.err.println("IOException " + e);
        }
    }*/
}
