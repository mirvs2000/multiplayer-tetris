import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.net.InetAddress;

public class PlayerTest {

    private Player player = new Player();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Test
    public void checkAddress() {
        InetAddress address = null;
        player.setAddress(address);
        assertThat(player.getAddress(), is(address));
    }

    @Test
    public void checkPort() {
        int port = 1;
        player.setPort(port);
        assertThat(player.getPort(), is(port));
    }

    @Test
    public void checkId() {
        byte id = 1;
        player.setId(id);
        assertThat(player.getId(), is(id));
    }

    @Test
    public void checkReadiness() {
        boolean read = true;
        player.setReadiness(read);
        assertThat(player.getReadiness(), is(read));
    }

    @Test
    public void checkStart() {
        boolean s = true;
        player.setStart(s);
        assertThat(player.getStart(), is(s));
    }

    @Test
    public void checkComboNum() {
        int cNum = 1;
        player.setComboNum(cNum);
        assertThat(player.getComboNum(), is(cNum));
    }

    @Test
    public void checkComboRec() {
        boolean cRec = true;
        player.setComboRec(cRec);
        assertThat(player.getComboRec(), is(cRec));
    }
}
